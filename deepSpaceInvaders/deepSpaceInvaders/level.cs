﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace deepSpaceInvaders
{
    public partial class level : Form
    {
        //global vars
        private static bool moveLeft, moveRight, enemyRight = true, enemyLeft, gameOver = false, isPressed, killedAll;
        //array of enemy pictures
        public static PictureBox[,] enemy = new PictureBox[4, 11];
        private static int numKilled = 0, timesWon = 1, enemyNum = 44;
        private static double enemySpeed = 0;
        public static int score;
        private const int PLAYER_SPEED = 5, HATCH_SPEED = 4;

        public level()
        {
            InitializeComponent();
        }

        //happens when form loads
        private void level_Load(object sender, EventArgs e)
        {
            score = 0;
            timesWon = 1;
            this.CenterToScreen();
            //creates enemies
            enemies(4,11);
            enemySpeed = timesWon * 3;
            numKilled = 0;
            killedAll = false;
            gameOver = false;
        }

        //main game timer
        private void gameTimer_Tick(object sender, EventArgs e)
        {
            lblScore.Text = "Score: " + score;
            stopGame();
            movePlayer();
            moveBullets();
            moveEnemies();
            killEnemy();
            endGame();
        }

        //handles enemy shooting on a seperate timer because main timer is too fast
        private void shootTimer_Tick(object sender, EventArgs e)
        {
            enemyShoot();
        }

        //one method to move all hatches in the game at a time
        private void moveBullets()
        {
            foreach (Control x in this.Controls)
            {
                if (x.Tag == "hatch_enemy")
                {
                    x.Top += HATCH_SPEED;
                }
                if (x.Top > 475)
                    x.Dispose();
            }
            foreach (Control x in this.Controls)
            {
                if (x.Tag == "hatch")
                {
                    x.Top -= HATCH_SPEED;
                }
                if (x.Top < -10)
                    x.Dispose();
            }
        }

        //spawns enemies
        private void enemies(int row, int col)
        {
            int previousX = 53, previousY = 12;
            for (int y = 0; y < row; y++)
            {
                previousY += 56;
                previousX = 53;
                for (int x = 0; x < col; x++)
                {
                    enemy[y, x] = new PictureBox();
                    enemy[y, x].Image = Properties.Resources.First_Ball_50;
                    enemy[y, x].Location = new Point(previousX, previousY);
                    enemy[y, x].Tag = "enemy";
                    previousX += 56;
                    enemy[y, x].Size = new Size(50, 50);
                    this.Controls.Add(enemy[y, x]);
                    enemy[y, x].BringToFront();
                }
            }
        }

        //makes enemies shoot back
        private void enemyShoot()
        {
            int randomX = 0, randomY = 0;
            bool loop = true;
            Random rand = new Random();
            while (loop)
            {
                randomX = rand.Next(0, 3);
                randomY = rand.Next(0, 10);
                loop = enemy[randomX, randomY].IsDisposed;
                Application.DoEvents();
            }
                PictureBox bulletEnemy = new PictureBox();
                bulletEnemy.Image = Properties.Resources.hatch_bullet;
                bulletEnemy.Size = new Size(16, 16);
                bulletEnemy.Tag = "hatch_enemy";
                //adds the bullet
                this.Controls.Add(bulletEnemy);
                bulletEnemy.BringToFront();
                bulletEnemy.Left = enemy[randomX, randomY].Left + 25;
                bulletEnemy.Top = enemy[randomX, randomY].Top + 25;
        }

        //moves player
        private void movePlayer()
        {
            if (moveRight)
                playerPictureBox.Left += PLAYER_SPEED;
            else if (moveLeft)
                playerPictureBox.Left -= PLAYER_SPEED;

            //checks player bounds
            if (playerPictureBox.Right >= 785)
            {
                playerPictureBox.Left -= PLAYER_SPEED;
            }
            if (playerPictureBox.Left <= 0)
            {
                playerPictureBox.Left += PLAYER_SPEED;
            }
        }

        //checks if key is pressed
        private void keyIsDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
                moveLeft = true;
            if (e.KeyCode == Keys.Right)
                moveRight = true;
            if(e.KeyCode == Keys.Space && !isPressed)
            {
                isPressed = true;
                makePlayerBullet();
            }
        }

        //checks if key is up
        private void keyIsUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
                moveLeft = false;
            if (e.KeyCode == Keys.Right)
                moveRight = false;
            if (isPressed)
                isPressed = false;
        }

        //moves enemies side and down
        private void moveEnemies()
        {
            foreach (Control x in this.Controls)
            {
                if (x is PictureBox && x.Tag == "enemy")
                {
                    //checks bounds of balls
                    if (x.Right >= 785)
                    {
                        enemyRight = false;
                        enemyLeft = true;
                    }
                    else if (x.Left <= 0)
                    {
                        enemyRight = true;
                        enemyLeft = false;
                    }

                    if (enemyRight)
                    {
                        x.Left += Convert.ToInt32(enemySpeed);
                    }
                    else if (enemyLeft)
                    {
                        x.Left -= Convert.ToInt32(enemySpeed);
                    }

                    //moves balls down
                    if (x.Right >= 785)
                    {
                        foreach (Control ball in this.Controls)
                        {
                            if (ball.Tag == "enemy")
                                ball.Top += 10;
                        }
                    }
                }
            }
        }

        //checks to see if enemy has been shot
        private void killEnemy()
        {
            foreach(Control ball in this.Controls)
            {
                //x is ball, z is hatch(bullet)
                foreach (Control hatch in this.Controls)
                {
                    if(ball.Tag == "hatch" && hatch.Tag == "enemy")
                    { 

                        if (ball.Bounds.IntersectsWith(hatch.Bounds))
                        {
                            hatch.Dispose();
                            ball.Dispose();

                            // Iterate through the enemy array to check if the correct amount are left alive
                            int newDeadCount = 0;
                            for (int i = 0; i < 4; i++)
                            {
                                for (int xxxTentacion = 0; xxxTentacion < 11; xxxTentacion++)
                                {
                                    if (enemy[i, xxxTentacion].IsDisposed)
                                    {
                                        newDeadCount++;
                                    }
                                }
                            }
                            numKilled = newDeadCount;
                            score += 20;
                            lblKills.Text = "Kills: " + numKilled + "/44";

                            if (numKilled == enemyNum)
                            {
                                killedAll = true;
                                lblWinner.Visible = true;
                                lblWinner.Text = "LEVEL " + timesWon + " COMPLETE. STARTING LEVEL " + (timesWon + 1);
                                endGame();
                            }
                        }
                    }
                }
            }
        }

        //checks to see if the balls have gone too far, or if player gets shot.
        private void stopGame()
        {
            foreach (Control x in this.Controls)
            {
                    if (x is PictureBox && x.Tag == "enemy" && x.Bottom >= 450)
                    {
                    gameOver = true;
                    }
                    else if(x is PictureBox && x.Tag == "hatch_enemy" && x.Bounds.IntersectsWith(playerPictureBox.Bounds))
                    {
                    gameOver = true;
                    }
            }
        }

        //creates bullet for shooter
        private void makePlayerBullet()
        {
            PictureBox bullet = new PictureBox();
            bullet.Image = Properties.Resources.hatch_bullet;
            bullet.Size = new Size(16, 16);
            bullet.Tag = "hatch";
            bullet.Left = playerPictureBox.Left + 55;
            bullet.Top = playerPictureBox.Top + 20;
            this.Controls.Add(bullet);
            bullet.BringToFront();
        }

        //happens after game is won or lost
        private void endGame()
        {
            if (gameOver)
            {
                Close();
                gameTimer.Stop();
                optionMenu options = new optionMenu();
                options.ShowDialog();
            }
            else if (killedAll)
            {
                gameTimer.Stop();
                foreach (Control x in this.Controls)
                {
                    if (x.Tag == "hatch" || x.Tag == "enemy")
                    {
                        x.Dispose();
                    }
                }
                numKilled = 0;
                if (shootTimer.Interval == 500)
                    shootTimer.Interval = 250;
                else
                    shootTimer.Interval -= 500;
                timesWon++;
                enemySpeed = timesWon * 1.25;
                killedAll = false;
                gameTimer.Start();
                enemies(4, 11);
            }
        }
    }
}