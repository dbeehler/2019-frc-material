﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace deepSpaceInvaders
{
    public partial class HighScores : Form
    {
        public HighScores()
        {
            InitializeComponent();
        }

        private void HighScores_Load(object sender, EventArgs e)
        {
            listBox1.ScrollAlwaysVisible = false;
            int[] scores = new int[999];
            String[] names = new string[999];
            int[] sortedScores = new int[999];
            String[] sortedNames = new string[999];

            int x = 0;
            foreach (String line in File.ReadAllLines(@"C:\Users\Public\scores.txt"))
            {
                string[] parts = line.Split(',');
                names[x] = parts[0];
                scores[x] = int.Parse(parts[1]);
                x++;
            }

            for (int z = 0; z < scores.Length; z++)
            {
                sortedScores[z] = scores[z];
            }


            Array.Sort(sortedScores);
            Array.Reverse(sortedScores);

            //fix double iteration
            for (int i = 0; i < scores.Length; i++)
            {
                int namePosition = Array.IndexOf(scores, sortedScores[i]);
                sortedNames[i] = names[namePosition];
                listBox1.Items.Add(sortedNames[i] + " Score: " + sortedScores[i]);
            }
        }
    }
}