﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace deepSpaceInvaders
{
    public partial class mainMenu : Form
    {
        public mainMenu()
        {
            InitializeComponent();
        }

        private void mainMenu_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
        }

        private void playPicBox_Click(object sender, EventArgs e)
        {
            level level = new level();
            level.ShowDialog();
        }

        private void scorePixBox_Click(object sender, EventArgs e)
        {
            HighScores highScoreForm = new HighScores();
            highScoreForm.ShowDialog();
        }

        private void blinkTimer_Tick(object sender, EventArgs e)
        {
            if(playPicBox.Visible == true && scorePixBox.Visible == true)
            {
                playPicBox.Visible = false;
                scorePixBox.Visible = false;
            }
            else if (playPicBox.Visible == false && scorePixBox.Visible == false)
            {
                playPicBox.Visible = true;
                scorePixBox.Visible = true;
            }
        }
    }
}
